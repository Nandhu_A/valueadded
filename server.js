const express = require('express');
const body_parser = require('body-parser');
const logger = require('morgan');
const sql = require('mssql');
const http = require('http');
const cors = require('cors');
const session = require('express-session');
const jwt = require('jsonwebtoken')
var store;

const PORT = 4000;
const app = express();
//const user=require('./src/app/model/user');
app.use(cors());
app.use(logger('dev'));
app.use(body_parser.json())
app.use(session({ secret: 'ssshhhhh', saveUninitialized: true, resave: true }));


app.post('/validate', async(req, res) => {
    //console.log(req.body.username)
    const query = "SELECT * FROM [valueadded].[dbo].[user]  where username ='" + req.body.username + "' and password='" + req.body.password + "'";
    const data = await executeQuery(query);
    //var arr = []
    store = data[0];
    console.log(store);
    console.log(data)
        //arr[{ data }, { store }]
        //console.log(arr)
    res.send(data);
    sql.close()
})
app.post('/validatetoken', async(req, res) => {
    //console.log(req.body.username)
    const query = "SELECT * FROM [valueadded].[dbo].[user]  where username ='" + req.body.username + "' and password='" + req.body.password + "'";
    const data = executeQuery(query);
    let payloade = { subject: user.User_Id }
    let token = jwt.sign(payloade, 'Allspark')
    res.send(token);
    await sql.close()
})

app.post("/create", async(req, res) => {
    console.log(req.body.Session.Dept_Id);

    const query = "insert into valueadded.dbo.Course (CourseName,CourseCode,CourseDescription,CourseDuration,CourseStart,CourseEnd,CourseCredit,CourseStrength,CoursePayment,Dept_Id) values " +
        "('" + req.body.CourseName + "'," +
        "'" + req.body.CourseCode + "'," +
        "'" + req.body.CourseDescription + "'," +
        "'" + req.body.CourseDuration + "'," +
        "'" + req.body.CourseStart + "'," +
        "'" + req.body.CourseEnd + "'," +
        "'" + req.body.CourseCredit + "'," +
        "'" + req.body.CourseStrength + "'," +
        "'" + req.body.CoursePayment + "'," +
        "'" + store.Dept_Id + "')";
    //console.log(query);
    const data = await executeQuery(query)
    res.send(data)
    sql.close()


});
app.get('/selectall', async(req, res) => {
    const query = "select * from valueadded.dbo.Course";
    const data = await executeQuery(query)
    res.send(data)
    sql.close()

})
app.post('/detail', async(req, res) => {
    console.log(req.body.CourseId)
    const query = "select * from valueadded.dbo.Course where CourseId= '" + req.body.CourseId + "'";
    console.log(query);
    const data = await executeQuery(query)
    res.send(data)
    sql.close()

})
app.post('/register', async(req, res) => {
    console.log(req.body.CourseId)
    const query = "insert into valueadded.dbo.Registration (User_Id,CourseId) values " +
        "('" + store.User_Id + "'," +
        "'" + req.body.CourseId + "')";
    const data = await executeQuery(query)
    res.send(data)
    sql.close()

})
app.get('/allregisteration', async(req, res) => {
    const query = "select * from [Student] inner join [User] on [Student].StudentID= [User].username inner join [Registration] on Registration.User_Id = [User].User_Id inner join [Course] on Course.CourseId=[Registration].CourseId inner join [Department] on [Course].Dept_Id=[Department].Dept_Id";
    const data = await executeQuery(query)
    res.send(data)
    sql.close()
})
app.get('/deptregisteration', async(req, res) => {
    const query = "select * from [Student] inner join [User] on [Student].StudentID= [User].username inner join [Registration] on Registration.User_Id = [User].User_Id inner join [Course] on Course.CourseId=[Registration].CourseId where Course.Dept_Id='" + store.Dept_Id + "'";
    const data = await executeQuery(query)
    res.send(data)
    sql.close()
})
app.post('/check', async(req, res) => {
    const query = "select * from Course inner join Registration on Course.CourseId=Registration.CourseId where Course.CourseStart >='" + req.body.start + "' and Course.CourseEnd>='" + req.body.start + "'and Registration.User_Id='" + store.User_Id + "'";
    const data = await executeQuery(query)
    console.log(data)
    res.send(data)
    sql.close()
})
app.post('/Datesearch', async(req, res) => {
    console.log(req.body)
    const query = "select * from [User] inner join [Registration] on Registration.User_Id = [User].User_Id inner join [Course] on Course.CourseId=[Registration].CourseId inner join [Department] on [Course].Dept_Id=[Department].Dept_Id where Course.CourseStart >='" + req.body.from + "' and Course.CourseEnd<='" + req.body.to + "'";
    const data = await executeQuery(query)
    res.send(data)
    sql.close()
})
app.post('/DeptDatesearch', async(req, res) => {
    console.log(req.body)
    const query = "select * from [User] inner join [Registration] on Registration.User_Id = [User].User_Id inner join [Course] on Course.CourseId=[Registration].CourseId where Course.Dept_Id='" + store.Dept_Id + "' and Course.CourseStart >='" + req.body.from + "' and Course.CourseEnd<='" + req.body.to + "' ";
    const data = await executeQuery(query)
    res.send(data)
    sql.close()
})
app.get('/selectdept', async(req, res) => {
    const query = "select Short_Name from Department";
    const data = await executeQuery(query)
    res.send(data)
    sql.close()
})
app.post('/searchdept', async(req, res) => {
    console.log(req.body)
    const query = "select * from [Student] inner join [User] on [Student].StudentID= [User].username inner join [Registration] on Registration.User_Id = [User].User_Id inner join [Course] on Course.CourseId=[Registration].CourseId inner join [Department] on [Course].Dept_Id=[Department].Dept_Id where Department.Short_Name='" + req.body.Short_Name + "'";
    const data = await executeQuery(query)
    res.send(data)
    sql.close()
})
app.get('/selectcourse', async(req, res) => {
    const query = "select CourseName from Course where Course.Dept_Id='" + store.Dept_Id + "'";
    const data = await executeQuery(query)
    res.send(data)
    sql.close()
})
app.post('/searchcourse', async(req, res) => {
    console.log(req.body)
    const query = "select * from [User] inner join [Registration] on Registration.User_Id = [User].User_Id inner join [Course] on Course.CourseId=[Registration].CourseId where Course.Dept_Id='" + store.Dept_Id + "' and Course.CourseName='" + req.body.CourseName + "'";
    const data = await executeQuery(query)
    res.send(data)
    sql.close()
})
app.get('/getdeptwithcount', async(req, res) => {
    console.log(req.body)
    const query = "select CourseName,count(*) as count,CourseStrength from Course inner join Registration on Course.CourseId=Registration.CourseId inner join [Department] on [Course].Dept_Id=[Department].Dept_Id where [Course].Dept_Id='" + store.Dept_Id + "' group by [Course].CourseName,[Course].CourseStrength";
    const data = await executeQuery(query)
    res.send(data)
    sql.close()
})
app.post('/getdeptwithcourse', async(req, res) => {
    console.log(req.body)
    const query = "select * from [Student] inner join [User] on [Student].StudentID= [User].username inner join [Registration] on Registration.User_Id = [User].User_Id inner join [Course] on Course.CourseId=[Registration].CourseId where Course.Dept_Id='" + store.Dept_Id + "' and Course.CourseName='" + req.body.CourseName + "'";
    const data = await executeQuery(query)
    res.send(data)
    sql.close()
})
app.post("/createstudent", async(req, res) => {


    const query = "insert into valueadded.dbo.Student (StudentID,StudentName,Dept_Id,Section,Mail,Gender,DOB,Graduation,StartingYear,EndingYear) values " +
        "('" + req.body.StudentID + "'," +
        "'" + req.body.StudentName + "'," +
        "'" + req.body.Department + "'," +
        "'" + req.body.Section + "'," +
        "'" + req.body.Mail + "'," +
        "'" + req.body.Gender + "'," +
        "'" + req.body.DOB + "'," +
        "'" + req.body.Graduation + "'," +
        "'" + req.body.StartingYear + "'," +
        "'" + req.body.EndingYear + "')";
    //console.log(query);
    const query1 = "insert into valueadded.dbo.[User] (username,password,role,Dept_Id,Question) values " +
        "('" + req.body.StudentID + "'," +
        "'" + req.body.DOB + "','student'," +
        "'" + req.body.Department + "'," +
        "'" + req.body.SecurityAnswer + "')";
    const data1 = await executeQuery(query1)
    sql.close()
    const data = await executeQuery(query)
    res.send(data)
    sql.close()



});
app.get('/chechcourse', async(req, res) => {
    const query = "select CourseName from Course where CourseCode='" + req.body.code + "' "
    const data = await executeQuery(query)
    res.send(data)
    sql.close()
})
app.get('/selectdepartment', async(req, res) => {
    console.log(req.body)
    const query = "select * from [Department]";
    const data = await executeQuery(query)
    res.send(data)
    sql.close()
})
app.post('/checkRoll', async(req, res) => {
    console.log(req.body)
    const query = "select * from [Student] where StudentID='" + req.body.StudentID + "'"
    const data = await executeQuery(query)
    res.send(data)
    sql.close()
})
app.post('/fogerpass', async(req, res) => {
    console.log(req.body)
    const query = "UPDATE valueadded.dbo.[User] SET password = '" + req.body.NEWPASS + "' WHERE username ='" + req.body.ROllNO + "'and Question='" + req.body.QUESTION + "'"
    const data = await executeQuery(query)
    res.send(data)
    sql.close()
})
app.post('/checkmail', async(req, res) => {
    console.log(req.body)
    const query = "select * from [Student] where Mail='" + req.body.Mail + "'"
    const data = await executeQuery(query)
    console.log(data)
    res.send(data)
    sql.close()
})
app.get('/getregcourse', async(req, res) => {
    console.log(req.body)
    const query = "select * from [User] inner join [Registration] on Registration.User_Id = [User].User_Id inner join [Course] on [Course].CourseId = [Registration].CourseId where [User].User_Id='" + store.User_Id + "'";
    const data = await executeQuery(query)
    res.send(data)
    sql.close()
})
app.get('/getdeptcourse', async(req, res) => {
    console.log(req.body)
    const query = "select * from [Course] where Dept_Id='" + store.Dept_Id + "'";
    const data = await executeQuery(query)
    res.send(data)
    sql.close()
})

const config = {
    user: 'sa',
    password: 'nandhu',
    server: 'localhost\\MSSQLSERVER', // You can use 'localhost\\instance' to connect to named instance
    database: 'valueadded',

}
executeQuery = async(query) => {
    console.log(query);
    try {
        let pool = await sql.connect(config)
        let result1 = await pool.request()
            .query(query)
        return (result1.recordset);

    } catch (err) {
        return (err)
            // ... error checks
        console.log(err);


    }

}


http.createServer(app).listen(PORT, () => {
    console.log(`Server has been started successfully on port ${PORT}`);
});