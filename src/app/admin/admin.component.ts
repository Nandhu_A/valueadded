import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { DateSearch } from '../model/seachcomp';
import { Shortname } from '../model/shortname';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(private _auth:AuthService) { }
  DateSearch = new DateSearch()
  Shortname=new Shortname()
  
  searchdept=(dept)=>{
    this.Shortname.Short_Name=dept
    console.log(this.Shortname.Short_Name)
    this._auth.searchdept(this.Shortname).subscribe(
      dataa=>{
        this.data=dataa
        console.log(this.data)
        
      },
      err =>{
        console.log("Something Went Wrong");
        alert("Something Went Wrong")
      }
    );

  }

  search=()=>{
    
    this._auth.Datesearch(this.DateSearch).subscribe(
      dataa=>{
        this.data=dataa
        console.log(this.data)
        
      },
      err =>{
        console.log("Something Went Wrong");
        alert("Something Went Wrong")
      }
    );
  }
data
dept
  ngOnInit() {
    
    this._auth.allregisteration().subscribe(
      dataa=>{
        this.data=dataa
        console.log(this.data)
        this._auth.selectdept().subscribe(
          dataa=>{
            this.dept=dataa
            console.log(this.data)
            
          },
          err =>{
            console.log("Something Went Wrong");
            alert("Something Went Wrong")
          }
        );
        
      },
      err =>{
        console.log("Something Went Wrong");
        alert("Something Went Wrong")
      }
    );
   

  }

}
