import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { StudentDashboardComponent } from './student-dashboard/student-dashboard.component';
import { DepartmentDashboardComponent } from './department-dashboard/department-dashboard.component';
import { CourseCreationComponent } from './course-creation/course-creation.component';
import { CourseDetailsComponent } from './course-details/course-details.component';
import { AdminComponent } from './admin/admin.component';
import { DepartmentRegistrationComponent } from './department-registration/department-registration.component';
import { AuthguardGuard } from './authguard.guard';
import { ExcelDownloadComponent } from './excel-download/excel-download.component';
import { StudentRegistrationComponent } from './student-registration/student-registration.component';
import { RegisterCourseComponent } from './register-course/register-course.component';
import { DepartmentCourseComponent } from './department-course/department-course.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';


const routes: Routes = [{path:'login',component:LoginComponent},{path:'',redirectTo:"login",pathMatch:"full"},
{path:'student',component:StudentDashboardComponent,canActivate:[AuthguardGuard]},{path:'student/:id',component:CourseDetailsComponent,canActivate:[AuthguardGuard]},
{path:'department',component:DepartmentDashboardComponent,canActivate:[AuthguardGuard]},
{path:'courseCreation',component:CourseCreationComponent,canActivate:[AuthguardGuard]},{path:'admin',component:AdminComponent,canActivate:[AuthguardGuard]},
{path:'departmentreg',component:DepartmentRegistrationComponent,canActivate:[AuthguardGuard]},
{path:"exceldownloade",component:ExcelDownloadComponent,canActivate:[AuthguardGuard]},
{path:"studentreg",component:StudentRegistrationComponent},
{path:'registerCourse',component:RegisterCourseComponent,canActivate:[AuthguardGuard]},
{path:'departmentCourse',component:DepartmentCourseComponent,canActivate:[AuthguardGuard]},{path:'Forgetpassword',component:ForgetPasswordComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
