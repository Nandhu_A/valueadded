import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DepartmentDashboardComponent } from './department-dashboard/department-dashboard.component';
import { StudentDashboardComponent } from './student-dashboard/student-dashboard.component';
import{FormsModule}from "@angular/forms"
import {HttpClientModule} from '@angular/common/http';
  import { from } from 'rxjs';
import { CourseCreationComponent } from './course-creation/course-creation.component';
import { CourseDetailsComponent } from './course-details/course-details.component';
import { HeaderComponent } from './header/header.component';

import { AdminComponent } from './admin/admin.component';
import { DepartmentRegistrationComponent } from './department-registration/department-registration.component';
import { AuthguardGuard } from './authguard.guard';
import { ExcelDownloadComponent } from './excel-download/excel-download.component';
import { XlsExportService } from './xls-export.service';
import { StudentRegistrationComponent } from './student-registration/student-registration.component';
import { RegisterCourseComponent } from './register-course/register-course.component';
import { DepartmentCourseComponent } from './department-course/department-course.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DepartmentDashboardComponent,
    StudentDashboardComponent,
    CourseCreationComponent,
    CourseDetailsComponent,
    HeaderComponent,
    AdminComponent,
    DepartmentRegistrationComponent,
    ExcelDownloadComponent,
    StudentRegistrationComponent,
    RegisterCourseComponent,
    DepartmentCourseComponent,
    ForgetPasswordComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule

  ],
  providers: [AuthguardGuard,XlsExportService],
  bootstrap: [AppComponent]
})
export class AppModule { }
