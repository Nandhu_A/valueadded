import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ThrowStmt } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor( private _http : HttpClient,private router : Router) { }
  url = "http://localhost:4000/"
  username
  role
  session

  navigate = async(data,user) =>{
    this.username=user
    this.role=data;
  if(data==='student')
  {
    this.router.navigate(['student']);
  }
  else if(data==='admin'){
    this.router.navigate(['admin']);
  }
  else if(data==='department'){
    this.router.navigate(['department']);
  }
  else {
    alert("username and password is wrong");
  }
  
}


 validateUser = (user) =>{
   console.log(user);
   
   return this._http.post(this.url+'validate',user);
 }
 validatetoken=(user)=>{
  return this._http.post(this.url+'validatetoken',user);
 }
 create=(Course)=>{
   console.log(Course)
   return this._http.post(this.url+'create',Course,this.session);

 }
 select=()=>{
  return this._http.get(this.url+'selectall');
  }
  detail=(CourseId)=>{
    console.log(CourseId)
    return this._http.post(this.url+'detail',CourseId)
  } 
  register=(CourseId)=>{
    return this._http.post(this.url+'register',CourseId)
    
  }
  allregisteration=()=>{
    return this._http.get(this.url+'allregisteration')
  }
  deptregisteration=()=>{
    return this._http.get(this.url+'deptregisteration')
  }
  check=(Start)=>{
    console.log(Start)
    return this._http.post(this.url+'check',Start)
  }
  Datesearch=(DateSearch)=>{
    console.log(DateSearch)
    return this._http.post(this.url+'Datesearch',DateSearch);
 
  }
  DeptDatesearch=(DateSearch)=>{
    console.log(DateSearch)
    return this._http.post(this.url+'DeptDatesearch',DateSearch);
 
  }
  selectdept=()=>{
    return this._http.get(this.url+'selectdept')

  }
  searchdept=(dept)=>{
    console.log(dept)
    return this._http.post(this.url+'searchdept',dept);

  }
  selectcourse=()=>{
    return this._http.get(this.url+'selectcourse')
  }
  searchcourse=(dept)=>{
    console.log(dept)
    return this._http.post(this.url+'searchcourse',dept);

  }
  loggedin=()=>{
    this.session=sessionStorage.getItem("USER")
    
    console.log(this.session)
    return !!sessionStorage.getItem('USER')
  }
  getuser=()=>{
    
    return this.username
  }
  logout=()=>{
    sessionStorage.removeItem('USER')
  }
  getdept=()=>{
    if(this.role==='department'){
      return true;
    }
  }
  User=()=>{
    if(this.role==='student'){
      return true;
    }
  }
  getdeptwithcount=()=>{
    return this._http.get(this.url+'getdeptwithcount')
  }
  getdeptwithcourse=(Coursename)=>{
    
    return this._http.post(this.url+'getdeptwithcourse',Coursename)
  }
  createstudent=(Student)=>{
    console.log(Student)
    return this._http.post(this.url+'createstudent',Student);
 
  }
  Foget=(fogerpass)=>{
    console.log(fogerpass)
    return this._http.post(this.url+'fogerpass',fogerpass);
 
  }
  insertstudent=(Student)=>{
    console.log(Student)
    return this._http.post(this.url+'insertstudent',Student);
 
  }
  chechcourse=(code)=>{
    return this._http.get(this.url+'chechcourse',code)

  }
  selectdepartment=()=>{
    return this._http.get(this.url+'selectdepartment')

  }
  checkRoll=(rollno)=>{
    return this._http.post(this.url+'checkRoll',rollno)


  }
  checkmail=(mail)=>{
    return this._http.post(this.url+'checkmail',mail)


  }
  getregcourse=()=>{
    return this._http.get(this.url+'getregcourse')
  }
  getdeptcourse=()=>{
    return this._http.get(this.url+'getdeptcourse')
  }
}