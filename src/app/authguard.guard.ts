import { Injectable } from '@angular/core';
import {CanActivate,Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthguardGuard implements CanActivate {
  canActivate() {
    if(this._auth.loggedin()){
      return true;
    }else{
      this._route.navigate(['/login'])
      return false
    }
    
  }
  
  constructor(private _auth : AuthService,private _route:Router){}


  
}
