import { Component, OnInit } from '@angular/core';
import { Course } from '../model/course'
import { from } from 'rxjs';
import { DatePipe } from '@angular/common';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-course-creation',
  templateUrl: './course-creation.component.html',
  styleUrls: ['./course-creation.component.css']
})
export class CourseCreationComponent implements OnInit {

  constructor(private _auth: AuthService) { }

  Course = new Course()
  datedis='false'
  
Session=sessionStorage.getItem('USER')

  oncreate = () => {
    console.log(this.Session[0])
    this.Course.Session=sessionStorage.getItem('USER')
    this._auth.create(this.Course).subscribe(
      data => {
        alert("successfully created")

      },
      err => {
        console.log("Something Went Wrong");
        alert("Something Went Wrong")
      }
    );

  }
  
  checkdate=()=>{
    var GivenDate = this.Course.CourseStart;
var CurrentDate = new Date();
GivenDate = new Date(GivenDate);

if(GivenDate < CurrentDate){
    alert('Given date is less than the current date,So give correct date');
     this.datedis='true'
}else{
  this.datedis='false'
}

  }

  ngOnInit() {

  }

}
