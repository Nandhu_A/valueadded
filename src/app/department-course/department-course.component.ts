import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-department-course',
  templateUrl: './department-course.component.html',
  styleUrls: ['./department-course.component.css']
})
export class DepartmentCourseComponent implements OnInit {
  data: any;

  constructor(private _auth:AuthService) { }

  ngOnInit() {
    this._auth.getdeptcourse().subscribe(
      dataa=>{
        this.data=dataa
        console.log(this.data)
        
      },
      err =>{
        console.log("Something Went Wrong");
        alert("Something Went Wrong")
      }
    );
  }

}
