import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { DateSearch } from '../model/Datesearch';
import { Coursename } from '../model/coursename';
import { Course } from '../model/course';

@Component({
  selector: 'app-department-registration',
  templateUrl: './department-registration.component.html',
  styleUrls: ['./department-registration.component.css']
})
export class DepartmentRegistrationComponent implements OnInit {
  dept: Object;

  constructor(private _auth:AuthService) { }
  DateSearch = new DateSearch()
  Coursename = new Coursename()
  searchcourse=(dept)=>{
    this.Coursename.CourseName=dept
    console.log(this.Coursename.CourseName)
    this._auth.searchcourse(this.Coursename).subscribe(
      dataa=>{
        this.data=dataa
        console.log(this.data)
        
      },
      err =>{
        console.log("Something Went Wrong");
        alert("Something Went Wrong")
      }
    );

  }
  search=()=>{
    console.log()
    this._auth.DeptDatesearch(this.DateSearch).subscribe(
      dataa=>{
        this.data=dataa
       
        console.log(this.data)
        
      },
      err =>{
        console.log("Something Went Wrong");
        alert("Something Went Wrong")
      }
    );
  }
data
  ngOnInit() {
    this._auth.deptregisteration().subscribe(
      dataa=>{
        this.data=dataa
        this._auth.selectcourse().subscribe(
          dataa=>{
            this.dept=dataa
            console.log(this.dept)
            
          },
          err =>{
            console.log("Something Went Wrong");
            alert("Something Went Wrong")
          }
        );
        console.log(this.data)
        
      },
      err =>{
        console.log("Something Went Wrong");
        alert("Something Went Wrong")
      }
    );
  }

}
