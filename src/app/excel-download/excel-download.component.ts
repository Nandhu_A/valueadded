import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Coursename } from '../model/coursename';
import { XlsExportService } from '../xls-export.service';

@Component({
  selector: 'app-excel-download',
  templateUrl: './excel-download.component.html',
  styleUrls: ['./excel-download.component.css']
})
export class ExcelDownloadComponent implements OnInit {
  Coursename = new Coursename()

  constructor(private _auth:AuthService,private _xls:XlsExportService) { }
  download=(CourseName)=>{
    this.Coursename.CourseName=CourseName
    this._auth.getdeptwithcourse(this.Coursename).subscribe(
    dataa=>{
      this.dataa=dataa
      console.log(this.dataa)
      this._xls.exportAsExcelFile(this.dataa,'sample')

      
    },
    err =>{
      console.log("Something Went Wrong");
      alert("Something Went Wrong")
    }
  );
    
  }
data
dataa
  ngOnInit() {
    this._auth.getdeptwithcount().subscribe(
      dataa=>{
        this.data=dataa
        console.log(this.data)
        
      },
      err =>{
        console.log("Something Went Wrong");
        alert("Something Went Wrong")
      }
    );
  }

}
