import { Component, OnInit } from '@angular/core';
import{ User } from '../model/user';
  import { from } from 'rxjs';
import { AuthService } from '../auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user = new User();
  
  constructor(private _auth : AuthService) { }
  submitData = () =>{
    console.log(this.user)
    this._auth.validateUser(this.user).subscribe(
      data =>{
        console.log(data)
        try{
        this._auth.navigate(data[0].role,data[0].username)
        sessionStorage.setItem("USER",JSON.stringify(data[0]));
        sessionStorage.setItem("USER_ID",data[0].User_Id);
        sessionStorage.setItem(" DEPT_ID",data[0]. Dept_Id);
      }
      catch(e){
        alert("UserName or password is wrong")
      }
        
        
      },
      err =>{
        console.log("Something Went Wrong");
        alert("UserName or password is wrong")
      }
    );
    
  }
  ngOnInit() {
    
  }

}
