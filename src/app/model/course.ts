export class Course {
    
    CourseName : String;
    CourseCode:String;
    CourseDescription:String;
    CourseDuration:Number;
    CourseStart:Date;
    CourseEnd:Date;
    CourseCredit:number;
    CourseStrength:number;
    CoursePayment:number;
    Session



    constructor(){

    }

}
