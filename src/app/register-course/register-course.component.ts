import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-course',
  templateUrl: './register-course.component.html',
  styleUrls: ['./register-course.component.css']
})
export class RegisterCourseComponent implements OnInit {
  data: Object;

  constructor(private _auth:AuthService,private router:Router) { }
showdetails=(item)=>{
    this.router.navigate(['/student',item.CourseId]);

  }
  ngOnInit() {
    this._auth.getregcourse().subscribe(
      dataa=>{
        this.data=dataa
        console.log(this.data)
        
      },
      err =>{
        console.log("Something Went Wrong");
        alert("Something Went Wrong")
      }
    );
  }

}
