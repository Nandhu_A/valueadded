import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import{Router}from '@angular/router'

@Component({
  selector: 'app-student-dashboard',
  templateUrl: './student-dashboard.component.html',
  styleUrls: ['./student-dashboard.component.css']
})
export class StudentDashboardComponent implements OnInit {
data:any;
  constructor(private _auth : AuthService,private router:Router) { }
  showdetails=(item)=>{
    this.router.navigate(['/student',item.CourseId]);

  }

  ngOnInit() {
    this._auth.select().subscribe(
      dataa=>{
        this.data=dataa
        console.log(this.data)
        
      },
      err =>{
        console.log("Something Went Wrong");
        alert("Something Went Wrong")
      }
    );

  }

}
