import { Component, OnInit } from '@angular/core';
import { Student } from '../model/student';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-student-registration',
  templateUrl: './student-registration.component.html',
  styleUrls: ['./student-registration.component.css']
})
export class StudentRegistrationComponent implements OnInit {
  Student = new Student();
  dept: Object;
  data: Object;
  constructor(private _auth: AuthService) { }
  getvalue = (event) => {
    console.log(event.target.value)
    this.Student.Gender = event.target.value
    console.log(this.Student.Gender)
  }
  getgraduation = (value) => {
    this.Student.Graduation = value.target.value
    console.log(this.Student.Graduation)
  }
  setdept = (dept) => {
    this.Student.Department = dept

  }
  oncreate = () => {
    this.Student.Session = sessionStorage.getItem('DEPT_ID')
    console.log(this.Student.Session)
    this._auth.createstudent(this.Student).subscribe(
      data => {
        alert("successfully created")
      },
      err => {
        console.log("Something Went Wrong");
        alert("Something Went Wrong")
      }
    );

  }
  onblur = () => {
    this._auth.checkRoll(this.Student).subscribe(
      data => {
        this.data = data
        try {
          console.log(this.data[0].Sid)
          alert("You have alredy register")
        }
        catch (err) {
          
        }
      },
      err => {
        console.log("Something Went Wrong");
        alert("Something Went Wrong")
      }
    );

  }
  checkmail = () => {
    this._auth.checkmail(this.Student).subscribe(
      data => {
        this.data = data
        try {
          console.log(this.data[0].Sid)
          alert("You have alredy register")
        }
        catch (err) {
          
        }
      },
      err => {
        console.log("Something Went Wrong");
        alert("Something Went Wrong")
      }
    );

  }
  oncreatedata = () => {
    console.log(this.Student)
  }
  ngOnInit() {

    this._auth.selectdepartment().subscribe(
      dataa => {
        this.dept = dataa
        console.log(this.dept)

      },
      err => {
        console.log("Something Went Wrong");
        alert("Something Went Wrong")
      }
    );
  }

}
